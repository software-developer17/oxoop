/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class Board {
    private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private int count;
    private boolean win = false;
    private boolean draw = false;

    public Board(Player o, Player x) {
        this.currentPlayer = o;
        this.o = o;
        this.x = x;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }


    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }

    public int getCount() {
        return count;
    }
    
    private void switchPlayer() {
        if(this.currentPlayer ==o) {
            this.currentPlayer = x;
        } else {
            this.currentPlayer = o;
        }
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }
    
    public boolean setRowCol(int row, int col) {
        if(row>3||col>3||row<1||col<1) return false;
        if(this.table[row-1][col-1] != '-') return false;
        this.table[row-1][col-1] = currentPlayer.getSymbol();
        if(this.checkWin(this.table, this.currentPlayer.getSymbol(), row, col)) {
            updateStat();
            this.win = true;
            return true;
        }
        if(this.checkDraw()) {
            o.draw();
            x.draw();
            this.draw = true;
            return true;
        }
        this.count++;
        this.switchPlayer();
        return true;
    }

    private void updateStat() {
        if(this.currentPlayer == o) {
            o.win();
            x.loss();
        } else {
            o.loss();
            x.win();
        }
    }
    
    public boolean checkVertical(char[][] _table, char currentPlayer, int col) {
        for (int row = 0; row < _table.length; row++) {
            if (_table[row][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public boolean checkHorizontal(char[][] _table, char currentPlayer, int row) {
        for (int col = 0; col < _table.length; col++) {
            if (_table[row - 1][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }
    public boolean checkX(char[][] _table, char _currentPlayer) {
        if (checkX1(_table, _currentPlayer)) {
            return true;
        } else if (checkX2(_table, _currentPlayer)) {
            return true;
        }
        return false;

    }

    private boolean checkX1(char[][] _table, char _currentPlayer) {
        for (int i = 0; i < _table.length; i++) {
            if (_table[i][i] != _currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private boolean checkX2(char[][] _table, char _currentPlayer) {
        for (int i = 0; i < _table.length; i++) {
            if (_table[i][2 - i] !=_currentPlayer) {
                return false;
            }
        }
        return true;
    }
    
    public boolean checkDraw() {
        if(this.count == 8) return true;
        return false;
    }
    public boolean checkDrawOld(char[][] _table) {
        for (int i = 0; i < _table.length; i++) {
            for (int j = 0; j < _table.length; j++) {
                if (_table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    private boolean checkWin(char[][] _table, char currentPlayer, int row, int col) {
        if (checkVertical(_table, currentPlayer, col)) {
            return true;
        } else if (checkHorizontal(_table, currentPlayer, row)) {
            return true;
        } else if (checkX(_table, currentPlayer)) {
            return true;
        }
        return false;

    }
    
}
